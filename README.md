## rust-multithread-httpserver

> a multithreaded boilerplate for building microservices with postgres support implemented

This is a blazing fast frontend node for our microservice network

#### Features

* Multithreaded using actix and `future`s
* Shared database pool
* dotenv configuration
* Modularized handlers

#### Libraries

* Actix
* Actix-web
* r2d2
* r2d2-postgres
* Future


### Run

`cargo run`

